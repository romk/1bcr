The One Billion Row Challenge

https://www.morling.dev/blog/one-billion-row-challenge/

## Benchmarks

### Test computers

|                        | EC2 m7a.8xlarge | Desktop
| ---------------------- | --------------- | -----------------------
| cpu                    | AMD EPYC 9R14   | Ryzen 7 3700x
| ram                    | 128             | 32
| ssd                    | ramdisk         | Samsung SSD 970 EVO Plus 2TB


### Base test

|                        | EC2 m7a.8xlarge | Desktop
| ---------------------- | --------------- | -----------------------
| wc -l measurements.txt | 0m7.611s        | 0m6.008s
| wc (c)                 |                 | 0m18.391s
| wc (go)                |                 | 0m18.741s
| wc (python)            |                 | 1m28.250s

### Simple implementation

|                        | EC2 m7a.8xlarge | Desktop
| ---------------------- | --------------- | -----------------------
| c (judy)               | 2m35.669s       | 3m43.497s
| go                     | 2m18.299s       | 6m0.413s
| go (csv)               | 2m12.110s       | 4m32.782s
| python                 | 5m41.963s       | 5m46.267s

### Optimized implementation

|                        | EC2 m7a.8xlarge | Desktop
| ---------------------- | --------------- | -----------------------
| python (pyarrow)       | 0m6.396s        | 0m12.233s


### Clear system caches

```
# sync && echo 3 > /proc/sys/vm/drop_caches
```
