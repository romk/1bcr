//      1brc: Solution to the 1brc
//      Copyright (C) 2024  Roman Kindruk

// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <Judy.h>

#define CITY_SZ 256
#define MAX_CITIES 512

struct CityData
{
  float min, max, sum, mean;
  uint count;
};
typedef struct CityData CityData;

CityData cities[MAX_CITIES];

int
main(int argc, char* argv[])
{
  FILE *stream;
  size_t len;
  char *city;
  float temp;
  CityData* new_city = cities;
  CityData* c;
  Pvoid_t PJArray = (Pvoid_t)NULL;
  Word_t* PValue;
  char Index[CITY_SZ];
  ssize_t n;

  if (argc != 2) {
    fprintf(stderr, "Usage: %s <file>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  stream = fopen(argv[1], "r");
  if (stream == NULL) {
    perror("fopen");
    exit(EXIT_FAILURE);
  }

  for (len=CITY_SZ, city=malloc(len); (n=getdelim(&city, &len, ';', stream)) != -1; len=CITY_SZ) {
    city[n-1] = '\0';           // drop the trailing ';'
    fscanf(stream, "%f\n", &temp);

    PValue = (PWord_t)new_city;
    JSLI(PValue, PJArray, city);
    if (*PValue == 0) {		/* inserted a new city */
      new_city->min = new_city->max = new_city->sum = temp;
      new_city->count = 1;
      *PValue = (Word_t)new_city++;
    }
    else {
      c = (CityData*)*PValue;
      if (temp < c->min)
	c->min = temp;
      if (temp > c->max)
	c->max = temp;
      c->sum += temp;
      c->count++;
    }
  }

  Index[0] = '\0';
  JSLF(PValue, PJArray, Index);     // get first string
  while (PValue != NULL) {
    c = (CityData*)*PValue;
    c->mean = c->sum / c->count;
    JSLN(PValue, PJArray, Index);   // get next string
  }

  return EXIT_SUCCESS;
}
