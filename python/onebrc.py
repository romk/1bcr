#      1brc: Solution to the 1brc
#      Copyright (C) 2024  Roman Kindruk

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import pyarrow as pa
from pyarrow import csv

if __name__ == "__main__":
    table = csv.read_csv(
        sys.argv[1],
        read_options=csv.ReadOptions(column_names=["city", "temperature"]),
        parse_options=csv.ParseOptions(delimiter=";"),
        convert_options=csv.ConvertOptions(
            column_types={"city": pa.string(), "temperature": pa.float32()}
        ),
    )
    res = table.group_by("city").aggregate(
        [("temperature", "min_max"), ("temperature", "mean")]
    )
    print(len(res["city"]))
