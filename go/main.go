//      1brc: Solution to the 1brc
//      Copyright (C) 2024  Roman Kindruk

// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
)

type CityMeasurements struct {
	Min   float64
	Max   float64
	Sum   float64
	Mean  float64
	Count uint
}

func ParseMeasurements(f *os.File) map[string]CityMeasurements {
	mes := map[string]CityMeasurements{}
	reader := csv.NewReader(f)
	reader.Comma = ';'
	reader.FieldsPerRecord = 2
	reader.ReuseRecord = true
	for {
		r, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(-1)
		}
		city := r[0]
		temp, err := strconv.ParseFloat(r[1], 32)
		m := mes[city]
		if temp < m.Min {
			m.Min = temp
		}
		if temp > m.Max {
			m.Max = temp
		}
		m.Sum += temp
		m.Count++
		mes[city] = m
	}
	for c, m := range mes {
		m.Mean = m.Sum / float64(m.Count)
		mes[c] = m
	}

	return mes
}

func SolutionMap(fname string) int {
	f, err := os.Open(fname)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}
	mes := ParseMeasurements(f)
	f.Close()
	return len(mes)
}

func main() {
	r := SolutionMap(os.Args[1])
	fmt.Println(r)
}
